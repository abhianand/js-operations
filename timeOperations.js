//this function will run after 3 seconds or 3000 milliseconds
setTimeout(() => {
    console.log("This will run after 3 seconds")
}, 3000); //time in milliseconds

//this function will run every 3 seconds or 3000 milliseconds
setInterval(() => {
    console.log("This will run every 3 seconds")
}, 3000); //time in milliseconds