const names = [
    "Abhi",
    "Amisha",
    "Nitheesh TC",
    "Prasobh",
    "Remesh"
]

// find length of an array
const len = names.length //Array.length
console.log(len)

//for loop
for (let i = 0; i < names.length; i++) {

    console.log(i);
    //continue: break the current cycle and continue with the next cycle 
    if (i % 2 === 0) {
        continue;
    }

    //break: break the for loop;
    if (i > 3) {
        break;
    }

    console.log(names[i])
}

//for each
names.forEach((x1) => {
    console.log(x1)
})



const newValue = names.map((name) =>{
    return "Hello my name is" + name
})

console.log(newValue)


const myNumber = [
    1, 2, 3, 4, 5, 6 ,7, 8, 9, 3, 3
];

//filter
const evenNumber =  myNumber.filter((num) => {
    return num % 2 === 0
})

// console.log(evenNumber)

//find 
const myVale = myNumber.find((num) => {
    return num > 3
})
console.log(myVale)

//find index of
const ind = myNumber.findIndex((num) => {
    return num > 5
})

console.log(ind)

//for of
//for in
//while

