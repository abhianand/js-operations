


//var
console.log("--------------------Var--------------------")

var xyz = "This a var variable";
console.log(xyz)

var xyz = "This is another variable"
console.log(xyz)

console.log("")


//let and const
console.log("--------------------Let--------------------")

let letVar = "This is a let variable"
console.log(letVar)

// let letVar = "This is another let variable" // this will throw an error

letVar = "Im modifying let's value"
console.log(letVar)

console.log("")
console.log("--------------------Const--------------------")

const constVar = "This is a const variable"
console.log(constVar)

// constVar = "This is replacing the value" //this will throw an error
// console.log(constVar)

console.log("--------------------Value by reference--------------------")

let ab = 100; //3 byte
console.log("ab =>", ab);

let bc = ab;
console.log('bc =>', bc)

ab = 200

console.log("ab =>", ab)
console.log("bc =>", bc)


