const { default: axios } = require("axios");

//Function
async function sum(x, y) {
    let value = x + y
    return value
}

let sumOfNum = sum(2, 3)
// console.log(sumOfNum)

//nameless function
let multiple = function (a, b) {
    let mulVal = a * b;
    return mulVal;
}

const multipleResult = multiple(4, 5)
// console.log(multipleResult)

//arrow function
const subs = (c, d) => {
    return c - d
}
console.log(subs(4, 5));

//Self invoking function
(
    function (x, y) {
        // console.log("Hi I'm a self invoking function", x, y)
    }
)(1, 2)



const getUserData = async () => {
    console.log("Calling to server");
    const response = await axios.get('instagram.com/profile?username=remesh') // {data: [], status: 200, statusText: "OK", headers: {…}, config: {…}, …}
    console.log(response);
}

getUserData()